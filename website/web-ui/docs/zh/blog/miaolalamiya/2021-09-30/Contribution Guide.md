---
title: Fork模式代码贡献总结
date: 2021-09-30
tags: 
    - Fork模式
    - 贡献总结
archives: 2021-09
author: miaolalamiya
summary: 本文详细介绍了使用TortoiseGit来进行Fork模式的配置和开发过程
---
## 1.安装git

(1)访问Git[官网](https://git-scm.com/downloads)并下载安装包。

![安装git](./1.png)

(2)记住默认安装的路径，并拷贝出来，后面会用到。

![安装过程](./2.png)

(3)一路next默认安装到完成。

## 2.安装TortoiseGit(小乌龟)及语言包

(1)访问TortoiseGit[官网](https://tortoisegit.org/download/)并下载安装包。双击TortoiseGit安装包，一路下一步，直到安装完成。

![TortoiseGit安装](./3.png)

(2)双击TortoiseGit语言包，一路下一步，直到最后一步勾选选项后完成。

![TortoiseGit安装-1](./4.png)

![TortoiseGit安装-2](./5.png)
## 3.注册gitee账号

登录https://gitee.com/ ，点击右上角注册，按提示完成注册。

## 4.Fork openEuler文档仓

(1)用已注册的账号登录gitee后，访问https://gitee.com/openeuler/docs

(2)点击右上角的Fork，选择Fork到自己的个人仓

![Fork](./6.png)

(3)进入Fork后的个人仓

![个人仓](./7.png)

(4)点击克隆/下载，选择HTTPS，然后复制仓库路径

![克隆](./8.png)

(5)在自己的个人电脑的某个空文件夹下，右击-》克隆，在弹框内粘贴刚刚复制的路径，点击确定。静待git把仓库代码拉取完成到个人电脑仓库上。

![克隆到个人仓](./9.png)

(6)拉取完成后，本地目录下会显示绿色的git勾子。（绿色表示没有修改，红色表示代码被修改了但还没推送代码），如果没有显示绿勾，参考这篇博文设置。https://blog.csdn.net/qyl_0316/article/details/80794633

## 5.重命名本地仓库和设置远端仓库路径

(1)右击拉下来本地仓库文件夹->TortoiseGit->设置，重命名origin为local

![重命名本地仓](./10.png)

![重命名本地仓-1](./11.png)

(2)进入https://gitee.com/openeuler/docs ， 复制HTTPS链接

(4)在git-远端，新建一个remote分支，把复制的链接粘贴到URL和推送URL中，点击添加/保存

![新建分支](./12.png)

(5)在连续的弹框中点击“是”、“是”、“确定”、“关闭”
(6)此时有两个分支，其中local是fork仓库，remote是openEuler文档的远端仓库

## 6.创建公钥和配置公钥
(1)进入C盘-》用户-》个人用户名目录下，例如C:\Users\miaolalamiya

(2)右击-》git bash here，弹出git命令行窗口

(3)输入git config --global user.name "用户名"，其中用户名是你注册的gitee用户名，并按回车

(4)输入git config --global user.email "邮箱"，其中邮箱是你注册的gitee用的邮箱，并按回车

(5)输入ssh-keygen -t rsa -C "邮箱"，其中邮箱是你注册的gitee用的邮箱，并一路按回车

![配置公钥](./13.png)

(6)此目录下会多出一个.ssh文件夹，进入该文件夹

![.ssh文件夹](./14.png)

(7)此目录下会多出一个.ssh文件夹，进入文件夹，并显示扩展名：

![显示扩展名](./15.png)

(8)右击id_rsa.pub文件，在打开方式中，使用记事本打开。并且把里面的内容全选拷贝。

![拷贝](./16.png)

(9)进入https://gitee.com ，点击设置按钮

![设置](./17.png)

(10)选择SSH公钥，把刚刚复制的公钥粘贴到文本框里，点击确定。

![粘贴公钥](./18.png)

## 7.创建分支

(1)右击本地仓库的入口文件夹-》tortoiseGit-》创建分支

(2)使用issue号作为分支名，并且使用远端的master分支来创建，勾选切换新分支

![新建分支](./19.png)

(3)基于分支做修改

## 8.提交和推送代码
(1) 右击本地代码仓入口文件夹，点击Git提交。

(2) 在弹出的提交框了，输入修改的日志信息后，点击提交。

(3) 提交完成后，点击推送按钮，选择本地分支和远端分支，点击推送。

## 9.提交PR
(1)进入个人Fork路径，检查修改的内容是否已合入。

(2)单击“+Pull Request”。
![+Pull Request](./20.png)
(3)输入修改的详细信息，并单击“Create”。提交时注意commits需为1。

(4)提交完成等待commiter审核后合入。


