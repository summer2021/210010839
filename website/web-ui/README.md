# website

### Brief Introduction

Website is openEuler community contents management system base on [Vuepress](https://www.vuepress.cn/) framework, [default Theme for Vuepress](https://www.vuepress.cn/theme/default-theme-config.html) theme, which publish on https://openeuler.org. Now we are under developing. you are welcome to join us.

### directory structure
docs  
    ├─ .vuepress                   
    │   ├── api                     // api  
    │   ├── component               // vue component  
    │   ├── data                    // data maintained  
    │   ├── lang                    // language module  
    │   ├── libs                    // common utils  
    │   ├── public                  // static resources  
    │   ├── style                   // common style  
    │   ├── theme                   // theme default options  
    │   ├── config.js               // global config  
    │   ├── enhanceApp.js           // app level enhancements  
    │   ├── sitePlugin.js           // plugin  
    ├─ en                          // English content  
    ├─ zh                          // Chinese content  

### Debug

1. Install dependencies

```
npm install
```

2. Run Vuepress development

```
npm run dev
```

The website will serving on http://your-server-ip:8080, any change will take effect here.

3. Run Vuepress production

```
npm run build

```

Copy dist folder to web container

### Contribution

1. Fork the repository
2. Create Feature_xxx branch
3. Commit your code
4. Create Pull Request

### Contribution considerations

1. When adding a new page
- You need to set reasonable and semantic title, description and keywords of the page.
- Check whether there are 404 links, and delete them if any.
- The H1 tag of the page needs to be unique and clearly let users know important parts of the website.
- Rich internal links are needed to avoid Island pages.
- For image display, you need to set reasonable and semantic alt attribute.
- Delete empty lines from source code.
- The file name cannot be too long and semantic. The length of the overall URL is less than 100 characters.
- Use the href of the a tag in the page to jump. Do not use the method function to jump, so as to prevent the crawler from climbing

2. When creating a new blog
- Unified rules under blog directory / blog / {{author}} / {{date}} / {{filename & Resource}}
- The blog file name shall be in English and controlled within 30 characters.

### Get Help

- IRC： #openeuler-infra     
- Mail: infra@openeuler.org
